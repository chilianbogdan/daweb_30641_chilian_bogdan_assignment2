import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from './landingpage';
import AboutMe from './AboutMe/aboutme';
import Contact from './contact';
import Project from './project';
import News from './news';
import Coordinator from './coordinator';


const Main = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/contact" component={Contact} />
    <Route path="/project" component={Project} />
    <Route path="/news" component={News} />
    <Route path="/coordinator" component={Coordinator} />
  </Switch>
)

export default Main;
