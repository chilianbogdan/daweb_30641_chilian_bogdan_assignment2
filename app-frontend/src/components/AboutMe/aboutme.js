import React, { Component } from 'react';
import {Cell} from "react-mdl";
import {FormattedMessage} from "react-intl";


class About extends Component {
  render() {
    return(
      <div> <h2 style={{paddingTop: '2em'}}>Bogdan Chilian</h2>
          <h4 style={{color: 'grey'}}>Student</h4>
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          <p><FormattedMessage id="about.faculty"
                               defaultMessage="Faculty of Automation and Computer Science, Cluj-Napoca (Romania)"
                               description="Link on react page"/></p>
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          <h5><FormattedMessage id="about.address"
                                defaultMessage="Address"
                                description="Link on react page"/></h5>
          <p><FormattedMessage id="about.add"
                               defaultMessage="Satu Mare, County: Satu Mare, Street: Bobocului, UK.21, Ap.3"
                               description="Link on react page"/></p>
          <h5><FormattedMessage id="about.phone"
                                defaultMessage="Phone"
                                description="Link on react page"/></h5>
          <p>0752684224</p>
          <h5>Email</h5>
          <p>chilian.bogdan@gmail.com</p>
          {/*<h5>Web</h5>*/}
          {/*<p>mywebsite.com</p>*/}
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/></div>
    )
  }
}

export default About;
