import React, { Component } from 'react';
import './App.css';
import {Layout, Header, Navigation, Drawer, Content, Cell} from 'react-mdl';
import Main from './components/main';
import { Link } from 'react-router-dom';
//import { createI18n, I18nProvider, withI18n } from 'react-portfolio-master'
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
//export let variableOne;
const setLanguage=(language)=>{
    window.localStorage.setItem('language',language);
    window.location.reload();
}
class App extends Component {

  render() {

    return (

      <div className="demo-big-content">
    <Layout>
        <Header className="header-color" title={<Link style={{textDecoration: 'none', color: 'white'}} to="/"><FormattedMessage id="app.learn-react-link"
                                                                                                                                defaultMessage="My Profile"
                                                                                                                                description="Link on react page"/></Link>} scroll>
            <Navigation>
                <Link to="/"><FormattedMessage id="app.home"
                                               defaultMessage="Home"
                                               description="Link on react page"/></Link>
                <Link to="/news"><FormattedMessage id="app.news"
                                                   defaultMessage="News"
                                                   description="Link on react page"/></Link>
                <Link to="/project"><FormattedMessage id="app.project"
                                                      defaultMessage="About project"
                                                      description="Link on react page"/></Link>
                <Link to="/aboutme"><FormattedMessage id="app.about"
                                                      defaultMessage="About me"
                                                      description="Link on react page"/></Link>
                <Link to="/coordinator"><FormattedMessage id="app.coordinator"
                                                          defaultMessage="Coordinator"
                                                          description="Link on react page"/></Link>
                <Link to="/contact"><FormattedMessage id="app.contact"
                                                      defaultMessage="Contact"
                                                      description="Link on react page"/></Link>
                <button className="button" id="save" onClick={() =>setLanguage('ro')}>RO
                </button>
                <button className="button" id="save" onClick={() =>setLanguage('en')}>EN
                </button>

            </Navigation>
        </Header>
        <Drawer title={<Link style={{textDecoration: 'none', color: 'black'}} to="/"><FormattedMessage id="app.learn-react-link"
                                                                                                       defaultMessage="My Profile"
                                                                                                       description="Link on react page"/></Link>}>
            <Navigation>
                <Link to="/"><FormattedMessage id="app.home"
                                               defaultMessage="Home"
                                               description="Link on react page"/></Link>
                <Link to="/news"><FormattedMessage id="app.news"
                                                   defaultMessage="News"
                                                   description="Link on react page"/></Link>
                <Link to="/project"><FormattedMessage id="app.project"
                                                      defaultMessage="About project"
                                                      description="Link on react page"/></Link>
                <Link to="/aboutme"><FormattedMessage id="app.about"
                                                      defaultMessage="About me"
                                                      description="Link on react page"/></Link>
                <Link to="/coordinator"><FormattedMessage id="app.coordinator"
                                                          defaultMessage="Coordinator"
                                                          description="Link on react page"/></Link>
                <Link to="/contact"><FormattedMessage id="app.contact"
                                                      defaultMessage="Contact"
                                                      description="Link on react page"/></Link>

            </Navigation>
        </Drawer>
        <Content>
            <div className="page-content" />
            <Main/>
        </Content>
    </Layout>

</div>

    );
  }
}

export default App;
